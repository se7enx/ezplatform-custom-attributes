const path = require('path');

module.exports = (eZConfig, eZConfigManager) => {
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-js',
        newItems: [
            path.resolve(__dirname, '../public/js/alloyeditor/plugins/custom-attributes.js'),
            path.resolve(__dirname, '../public/js/alloyeditor/buttons/embed-view.js'),
        ]
    });
    eZConfigManager.add({
        eZConfig,
        entryName: 'ezplatform-admin-ui-alloyeditor-css',
        newItems: [
            path.resolve(__dirname, '../public/scss/alloyeditor/plugins/custom-attributes.scss'),
        ]
    });
};